//
// Created by guill on 20/10/2023.
//

#ifndef TP1_ARCHI_FILEWRITER_H
#define TP1_ARCHI_FILEWRITER_H


#include <string>

class FileWriter {

public:
    static void writeCSV( int tab[][4], int nbDay, int nbSimu);

};


#endif //TP1_ARCHI_FILEWRITER_H
